var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var exphbs = require('express-handlebars');

// var fs = require('fs');
// var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

var Config = require('./modules/config.js');
var config = new Config();

var app = express();

var Particle = require('particle-api-js');
var particle = new Particle();

var request = require('request');
var moment = require('moment');

var MaddoFCM = require('./modules/maddoFCM.js');
var Database = require('./modules/Database.js');
var Weather = require('./modules/WeatherDb.js');

var mongoose = require('mongoose');
mongoose.connect(config.DatabaseAddress);

//var Device = require('./models/device');

//mongoose.connect('mongodb://192.168.100.100/pcc');

app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

app.use(cookieParser());
app.use(bodyParser.json());



var IsLoggedIn = function (req) {
    return (req.cookies.particle_token);
};

var router = express.Router();
/*
router.use(function (req, res, next) {

    if (IsLoggedIn(req)) {
        next();
    }
    else {
        res.redirect('/login');
    }

});
*/
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

require('./routes/motionsensor.js')(app);

app.route('/API/Tokens/Login')
    .post(function (req, res) {
        // Need to process the login form
        var tokenDuration = req.body.tokenDuration;


        particle.login({
            username: req.body.email, // insert from post data
            password: req.body.password // insert from post data
        }).then(

            function (data) {
                console.log('api call completed on promise resolve: ' + data.body.access_token);

                res.JSON(data.body);

            },
            function (err) {
                console.log('api call completed on promise fail:', err);
                //res.send(err.statusCode, err);
                res.status(err.statusCode).send(err.body);
            });

    });

app.route('/API/Tokens/List')
    .post(function (req, res) {

        particle.listAccessTokens({
            username: req.body.email, // insert from post data
            password: req.body.password // insert from post data
        }).then(function (data) {
            console.log('data on listing access tokens: ', data);
            res.JSON(data.body);

        }, function (err) {
            console.log('error on listing access tokens: ', err);
            res.status(err.statusCode).send(err.body);
        });
    });


// router.get('/API/MaddoDevices/MotionSensor/Notify', function (req, res) {

//     // var address = "https://api.pushover.net/1/messages.json";

//     // request.post(address, function (err, incomingMessage, response) {
//     //     // Callback Function
//     //     if (err) {
//     //         //res.status(400).send(err.code);
//     //     } else {
//     //         //res.status(200).send();
//     //     }

//     // }).form({
//     //     token: config.pushoverAppKey,
//     //     user: config.pushoverUserKey,
//     //     message: "Movement Detected"
//     // });
//     //////// FCM
//     var message = {
//         to: config.TestDeviceToken,
//         //collapse_key: "thefuckisthis",
//         /*notification: {
//             body: "lmao"
//         },*/
//         data: {
//             content: "lmao"
//         },
//         "time_to_live": 10

//     };

//     fcm.send(message, function (err, response) {
//         if (err) {
//             console.log(err);
//             //res.status(400).send();
//         }
//         else {
//             console.log(`Notification sent to firebase successfully [${Date.now()}]`);
//             //res.status(200).send();
//         }
//     });

//     res.status(200).send();

// });

//router.post('/API/MaddoDevices/MotionSensor/Notify', function (req, res) {

//    var type = req.body.type;
//    MaddoFCM.SendMessage(type);

//    res.status(200).send();

//});

router.get('/API/Weather/Data', function (req, res) {
    Weather.GetData(req, res).then(function (result) {
        res.status(200).send(JSON.stringify(result));
    }, function (err) {
        console.log('error: ', err);
        res.status(err.statusCode).send(err.body);
    }
    );
});

router.get('/API/Weather/LastDay', function (req, res) {

    var now = new Date();
    var lastDay = moment(now).subtract(24, "h").toDate();

    Weather.GetRange(lastDay, now).then(function (result) {
        res.status(200).send(JSON.stringify(result));
    }, function (err) {
        console.log('error: ', err);
        res.status(err.statusCode).send(err.body);
    }
    );
});

router.get('/API/Weather/Now', function (req, res) {
    Weather.GetCurrentData().then((data) => {
        res.status(200).send(JSON.stringify(data));
    })
        .catch((err) => {
            res.status(500).send(err.body);
        });
});

router.get('/API/Weather/Clear', function (req, res) {
    Weather.ClearData().then(function (result) {
        res.status(200).send(JSON.stringify(result));
    }, function (err) {
        console.log('error on listing access tokens: ', err);
        res.status(500).send(err.body);
    }
    );
});

router.post('/API/register',
    function (req, res) {
        console.log(`Received notification code: ${req.params.token}`);

        RegisterDevice(req.body.name, req.body.token, req.body.type, req, res);
    });

router.post('/devices/function', function (req, res) {
    executeFunction(req, res, req.body.token,
        req.body.deviceId,
        req.body.functionName,
        req.body.functionParameter);
});

router.get('/notificationstest', function (req, res) {
    res.render('notifications');
});

var executeFunction = function (req, res, token, deviceId, functionName, functionParameter) {
    var fnPr = particle.callFunction({
        deviceId: deviceId,
        name: functionName,
        argument: functionParameter, //'D0:HIGH',
        auth: token
    });

    fnPr.then(
        function (data) {
            console.log('Function called succesfully:', data);
            res.send(JSON.stringify(data));
        }, function (err) {
            console.log('An error occurred:', err);
            //res.redirect('/error');
            res.status(400).send(JSON.stringify(err));
        });
};


var RegisterDevice = function (deviceName, token, type, req, res) {

    Database.RegisterDevice(deviceName, token, type, req, res);

}

router.get('/web/notifications', function (req, res) {

    res.render('notifications');

});

app.use(express.static('public'));

/*
app.route('/login')
    .get(function (req, res) {
        res.render('login');
    })
    .post(function (req, res) {
        // Need to process the login form

        particle.login({
            username: req.body.email, // insert from post data
            password: req.body.password // insert from post data
        }).then(

            function (data) {
                console.log('api call completed on promise resolve: ' + data.body.access_token);
                res.cookie('particle_token', data.body.access_token,
                    { maxAge: 900000, httpOnly: true })//.send("Cookie Set");;
                    .redirect('/panel');

            },
            function (err) {
                console.log('api call completed on promise fail:', err);
                res.redirect('/error');
            });

    });



router.get('/error', function (req, res) {
    res.render('error');
});

router.get('/panel', function (req, res) {
    res.render('panel');
})

router.get('/', function (req, res) {
    res.render('home');
});

router.get('/devices/list', function (req, res) {

    var devicesPr = particle.listDevices({ auth: req.cookies.particle_token });

    devicesPr.then(
        function (devices) {
            console.log('Devices: ', devices);

            res.render('devices', {
                devices: devices.body
            });

            //res.send(JSON.stringify(devices));
        },
        function (err) {
            console.log('List devices call failed: ', err);
            res.redirect('/error');
        }
    );

});

router.get('/devices/:deviceName/status', function (req, res) {
    var devicesPr = particle.getDevice({
        deviceId: req.params.deviceName,
        auth: req.cookies.particle_token
    });

    devicesPr.then(
        function (data) {
            console.log('Device attrs retrieved successfully:', data);
            res.render('deviceStatus', {
                data: data.body,
                rawJSON: JSON.stringify(data)
            });
            //res.send(JSON.stringify(data));
        },
        function (err) {
            console.log('API call failed: ', err);
            res.redirect('/error');
        }
    );
});


*/
app.use('/', router);


app.listen(config.listenPort);
console.log(`Listening on Port: ${config.listenPort}`);

Weather.CallApi();