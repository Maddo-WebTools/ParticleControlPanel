var express = require('express');
var router = express.Router();
var FCM = require('fcm-node');
var fcm = new FCM(config.FCMServerKey);

router.post('/API/MaddoDevices/MotionSensor/Notify', function (req, res) {

    var type = req.body.type;

    // todo: recuperare tutti i token iscritti a quel determinato tipo e sparargli notifiche

    var message = {
        to: `/topics/${type}`,
        //collapse_key: "thefuckisthis",
        /*notification: {
            body: "lmao"
        },*/
        data: {
            content: type
        },
        "time_to_live": 10

    };
    // todo: differentiate based on notification type
    fcm.send(message, function (err, response) {
        if (err) {
            console.log(err);
            //res.status(400).send();
        }
        else {
            console.log(`Notification sent to firebase for type ${type} successfully [${Date.toString()}]`);
            //res.status(200).send();
        }
    });

    res.status(200).send();

});