// JavaScript source code
var Config = require('./config.js');
var config = new Config();

var mongoose = require('mongoose');
//mongoose.connect(config.DatabaseAddress);
var Service = require('../models/service');

mongoose.Promise = global.Promise;
/**
 * Returns data on the selected service and if it's enabled or disabled
 * @param {} name - the name of the service
 * @returns {} Result promise
 */
module.exports.GetServiceData = function (name) {
    return Service.findOne({ name: name }).exec();
}
/**
 * Return a list of all registered services
 * @returns {} 
 */
module.exports.GetAllServices = function () {
    return Service.find({ type: "Service" }).exec();
}

module.exports.ToggleService = function (name, status) {
    var query = { name: name },
        update = { active: status },
        options = { upsert: true, new: true, setDefaultsOnInsert: true };

    return Service.findOneAndUpdate(query, update, options).exec();
}

handleError = function (err) {
    console.log(error);
}

module.exports.RegisterDevice = function (deviceName, token, type, req, res) {
    var dev = new Device({
        name: deviceName,
        token: token,
        active: true,
        notificationTypes: [type]
    });

    var dbQuery = { token: token };


    Device.findOne({ token: token }, function (err, device) {
        if (err) {
            console.log(err);
            res.status(400).send(JSON.stringify({ status: "fail", error: JSON.stringify(err) }));
        }
        else {
            if (device) {
                // device exists

                Device.findOne({ token: device.token, notificationTypes: type }, function (err, a) {
                    if (err) {
                        console.log(err);
                        res.status(400).send(JSON.stringify({ status: "fail", error: JSON.stringify(err) }));
                    } else {
                        if (a) {
                            // successfull
                            res.status(200).send(JSON.stringify({ status: "success", details: "Device was already in the database with this type of notification" }));
                        }
                        else {
                            // type is not there, add it
                            device.notificationTypes.push(type);
                            device.save(function (err) {
                                if (err) {
                                    console.log(err)

                                    res.status(400).send(JSON.stringify({ status: "fail", error: JSON.stringify(err) }));
                                    return;
                                };
                                console.log(`Updated token ${device.token} with type ${type}`);
                                res.status(200).send(JSON.stringify({ status: "success", details: "Added notification type to device" }));
                            })
                        }
                    }

                });



            }
            else {
                // device doesn't exist, add it
                dev.save(function (err) {
                    // no fucking idea what to do in case of error
                    if (err) {
                        console.log(err);
                        res.status(400).send(JSON.stringify({ status: "fail", error: JSON.stringify(err) }));
                        return;
                    }

                    console.log(`Device added: ${dev.token}`);
                    res.status(200).send(JSON.stringify({ status: "success", details: "Added new device" }));
                });
            }
        }
    })

}