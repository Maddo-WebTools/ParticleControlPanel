var Config = require('./config.js');
var config = new Config();

var FCM = require('fcm-node');
var fcm = new FCM(config.FCMServerKey);


//var exports = module.exports = {};

module.exports.SendMessage = function (type) {

    var message = {
        to: `/topics/${type}`,
        //collapse_key: "thefuckisthis",        
        data: {
            content: type
        },
        "time_to_live": 10
    };

    if (type === "Movement") {
        message.data.soundname = "msgalert"
    }
    else if (type === "Doorbell") {
        message.data.soundname = "doorbell"
    }

    // todo: differentiate based on notification type
    fcm.send(message, function (err, response) {
        if (err) {
            console.log(err);
            //res.status(400).send();
        }
        else {
            console.log(`Notification sent to firebase for type ${type} successfully [${Date.toString()}]`);
            //res.status(200).send();
        }
    });


}

