// JavaScript source code
var Config = require('./config.js');
var config = new Config();

var moment = require('moment');

var mongoose = require('mongoose');
//mongoose.connect(config.DatabaseAddress);
var Weather = require('../models/weather');

mongoose.Promise = global.Promise;

var request = require('request');
var schedule = require('node-schedule');

module.exports.CallApi = function () {

    var timer = config.WeatherPollingTime * 1000;
    console.log("Trying to start request timer");

    var rule = new schedule.RecurrenceRule();
    rule.minute = 0;

    var j = schedule.scheduleJob(rule, function () {
        console.log("Doing request");
        request(config.WeatherAddress, function (error, response, body) {
            if (!error && response.statusCode == 200) {

                try {
                    var data = JSON.parse(body);

                    var w = RequestToWeather(data);

                    w.save(function (err) {
                        // no fucking idea what to do in case of error
                        if (err) {
                            console.log(err);

                            return;
                        }

                        console.log(`Data registered`);

                    });

                    console.log(JSON.stringify(data));
                }
                catch (e) {
                    console.error(e);
                }

            }
        })
    });

    // setInterval(function () {
    //     console.log("Doing request");
    //     request(config.WeatherAddress, function (error, response, body) {
    //         if (!error && response.statusCode == 200) {

    //             try {
    //                 var data = JSON.parse(body);

    //                 var w = RequestToWeather(data);

    //                 // var w = new Weather({
    //                 //     winddir: data.winddir,
    //                 //     windspeedkmh: data.windspeedkmh,
    //                 //     windgustkmh: data.windgustkmh,
    //                 //     windgustdir: data.windgustdir,
    //                 //     humidity: data.humidity,
    //                 //     tempf: data.tempf,
    //                 //     tempc: data.tempc,
    //                 //     tempExtC: data.tempExtC,
    //                 //     //rainin: data.rainin,
    //                 //     rainmm: data.rainmm,
    //                 //     dailyrainin: data.dailyrainin,
    //                 //     dailyrainmm: data.dailyrainmm,
    //                 //     pressure: data.pressure,
    //                 //     batt_lvl: data.batt_lvl,
    //                 //     light_lvl: data.light_lvl,
    //                 // });

    //                 w.save(function (err) {
    //                     // no fucking idea what to do in case of error
    //                     if (err) {
    //                         console.log(err);

    //                         return;
    //                     }

    //                     console.log(`Data registered`);

    //                 });

    //                 console.log(JSON.stringify(data));
    //             }
    //             catch (e) {
    //                 console.error(e);
    //             }

    //         }
    //     })
    // }, timer);
}

RequestToWeather = function (data) {
    return new Weather({
        winddir: data.winddir,
        windspeedkmh: data.windspeedkmh,
        windgustkmh: data.windgustkmh,
        windgustdir: data.windgustdir,
        humidity: data.humidity,
        tempf: data.tempf,
        tempc: data.tempc,
        tempExtC: data.tempExtC,
        //rainin: data.rainin,
        rainmm: data.rainmm,
        dailyrainin: data.dailyrainin,
        dailyrainmm: data.dailyrainmm,
        pressure: data.pressure,
        batt_lvl: data.batt_lvl,
        light_lvl: data.light_lvl,
    });
}

module.exports.GetData = function (req, res) {
    return Weather.find({}).exec();
}

module.exports.GetLastDay = function () {

    var now = new Date();
    var lastDay = moment(now).subtract(24, "h").toDate();

    var query = {
        'time': {
            '$gte': lastDay,
            '$lte': now
        }
    };

    return Weather.find(query).exec();
}

module.exports.GetRange = function (start, end) {
    var query = {
        'time': {
            '$gte': start,
            '$lte': end
        }
    };

    return Weather.find(query).exec();
}

module.exports.ClearData = function () {
    return Weather.remove({}).exec();
}

module.exports.GetCurrentData = function () {

    return new Promise((resolve, reject) => {

        request(config.WeatherAddress, function (error, response, body) {
            if (!error && response.statusCode == 200) {

                try {
                    var data = JSON.parse(body);

                    var w = RequestToWeather(data);

                    resolve(w);

                    console.log(JSON.stringify(data));
                }
                catch (e) {
                    reject(e);
                    console.error(e);
                }

            }
            else {
                reject(error);
            }
        });

    })



}