var fs = require('fs');


function Config() {
    var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
    return config;
}

module.exports = Config;