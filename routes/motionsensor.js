﻿var MaddoFCM = require('../modules/maddoFCM.js');
var Database = require('../modules/Database.js');

module.exports = function (router) {

    router.post('/API/MaddoDevices/Status',
        function (req, res) {

            console.log("Sending data");
            var type = req.body.type;

            Database.GetServiceData(type)
                .then(function (doc) {
                    console.log(JSON.stringify(doc));
                    res.status(200).send(JSON.stringify(doc));
                }).catch(function (err) {
                    console.log(err);
                    res.status(400).send(JSON.stringify({ error: { description: err } }));
                });

            //            res.status(200).send(JSON.stringify(r));

        });

    router.get('/API/MaddoDevices/All',
        function (req, res) {
            //Promise.all(promises).then(function(results) { ... });
            Database.GetAllServices()
                .then(function (results) {

                    console.log(JSON.stringify(results));
                    res.status(200).send(JSON.stringify(results));
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(400).send(JSON.stringify({ error: { description: err } }));
                });
        });

    router.post('/API/MaddoDevices/Notify',
        function (req, res) {
            console.log("Notification engaged");
            // todo: check type
            // check if active
            var type = req.body.type;
            // Database.GetServiceData(type)
            //     .then(function(result) {
            //         if (result) {
            //             if (result.active) {
            //                 MaddoFCM.SendMessage(type);
            //             }
            //         }
            //     })
            //     .catch(function(err) {
            //         console.log(err);
            //     });

            if (type) {
                MaddoFCM.SendMessage(type);
            }

            res.status(200).send();

        });

    // API:
    // name: the name (string)
    // status: the status (bool)
    router.post('/API/MaddoDevices/Toggle',
        function (req, res) {
            var name = req.body.name;
            var status = req.body.status;
            //todo: name validation
            if (status !== true && status !== false) {
                console.log("invalid request");
                res.status(400).send(JSON.stringify({ error: { description: "Bad status" } }));
                return;
            }

            Database.ToggleService(name, status).then(function (result) {
                console.log(JSON.stringify(result));
                res.status(200).send(JSON.stringify(result));
            }).catch(function (err) {
                console.log(err);
                res.status(400).send(JSON.stringify({ error: { description: err } }));
            });

        });


}