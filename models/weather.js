﻿var mongoose = require('mongoose');
//require('mongoose-double')(mongoose);
var SchemaTypes = mongoose.Schema.Types;

/*var weatherSchema = new mongoose.Schema({
    winddir: Number,
    windspeedkmh: SchemaTypes.Double,
    windgustkmh: SchemaTypes.Double,
    windspdmph_avg2m: SchemaTypes.Double,
    winddir_avg2m: SchemaTypes.Double,
    windgustmph_10m: SchemaTypes.Double,
    wigustdir_10m: SchemaTypes.Double,
    humidity: SchemaTypes.Double,
    tempf: SchemaTypes.Double,
    tempc: SchemaTypes.Double,
    time: { "type": Date, "default": Date.now }
}, { collection: 'devices' });*/

var weatherSchema = new mongoose.Schema({
    winddir: Number,
    windspeedkmh: Number,
    windgustkmh: Number,
    windgustdir: Number,
    humidity: Number,
    tempf: Number,
    tempc: Number,
    tempExtC: Number,
    //rainin: Number,
    rainmm: Number,
    dailyrainin: Number,
    //dailyrainmm: Number,
    pressure: Number,
    batt_lvl: Number,
    light_lvl: Number,
    time: { "type": Date, "default": Date.now },
}, { collection: 'weatherData', timestamps: true });

var Weather = mongoose.model('Weather', weatherSchema);

module.exports = Weather;


// {
//   "winddir": 225,
//   "windspeedmph": 0.0,
//   "windgustkmh": 108.3,
//   "windgustdir": 270,
//   "windspdmph_avg2m": 0.0,
//   "winddir_avg2m": 225,
//   "windgustmph_10m": 0.0,
//   "wigustdir_10m": 0,
//   "humidity": 6.0,
//   "tempf": 100.3,
//   "tempc": 37.9,
//   "rainin": 0.07,
//   "dailyrainin": 0.24,
//   "pressure": 1.01e5,
//   "batt_lvl": 10.31,
//   "light_lvl": 0.05
// }
