﻿var mongoose = require('mongoose');

var serviceSchema = new mongoose.Schema({
    name: String,
    active: Boolean,
    type: { "type": String, "default": "Service" },
    added: { "type": Date, "default": Date.now }
}, { collection: 'services' });

var Service = mongoose.model('Device', serviceSchema);

module.exports = Service;