var mongoose = require('mongoose');

var deviceSchema = new mongoose.Schema({
    name: String,
    token: { type: String, required: true, unique: true },
    active: Boolean,
    notificationTypes: {
        "type": Array, "default": []
    },
    added: { "type": Date, "default": Date.now }
}, { collection: 'devices' });

var Device = mongoose.model('Device', deviceSchema);

module.exports = Device;